import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path:'inicio',
    component:HomeComponent
  },
  {
    path:'tareas',
    loadChildren:() => import('./tareas/tareas.module').then(m => m.TareasModule).catch(error=>console.log('fallo al cargar tareas module'))    
  },
  {
    path:'',
    component:HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
