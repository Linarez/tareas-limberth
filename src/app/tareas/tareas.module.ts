import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule,MatButtonModule,MatInputModule } from '@angular/material';
import { FormsModule,ReactiveFormsModule } from "@angular/forms";

import { TareasRoutingModule } from './tareas-routing.module';
import { InicioComponent } from './inicio/inicio.component';
import { Tarea1Component } from './tarea1/tarea1.component';
import { Tarea2Component,ListaCurso,CrearCurso,ItemCurso } from './tarea2/tarea2.component';
import { ComunicationService } from '../services/comunication.service';

@NgModule({
  declarations: [InicioComponent, Tarea1Component, Tarea2Component,ListaCurso,CrearCurso,ItemCurso],
  imports: [
    CommonModule,
    TareasRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers:[
    ComunicationService
  ]
})
export class TareasModule { }
