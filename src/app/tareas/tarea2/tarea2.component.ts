import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { FormBuilder,FormGroup,Validators } from '@angular/forms';
import { Curso } from '../../interfaces/curso';
import { ComunicationService } from 'src/app/services/comunication.service';

@Component({
  selector: 'app-tarea2',
  templateUrl: './tarea2.component.html',
  styleUrls: ['./tarea2.component.css']
})
export class Tarea2Component implements OnInit {
  cursos:Curso[];
  data2Form:any;
  constructor(private comunicationService:ComunicationService) {
    this.data2Form = {option:'add',title:'Nuevo curso',data:null,ind:0};
    this.cursos = [
      {        
        nombre:'Angular',
        descripcion:'Angular es un framework',
        imagen:'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/800px-Angular_full_color_logo.svg.png'
      },
      {       
        nombre:'React.js',
        descripcion:'React.js es una libreria',
        imagen:'https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/React.svg/800px-React.svg.png'
      },
      {       
        nombre:'Vue.js',
        descripcion:'Vuejs es un framework',
        imagen:'https://vuejs.org/images/logo.png'
      },
      // {
      //   id:4,
      //   nombre:'Laravel',
      //   descripcion:'Laravel es un framework php',
      //   imagen:'https://www.theblocklearning.com/wp-content/uploads/2018/09/laravel-logo-big-1.png'
      // }
    ];
  }

  ngOnInit() {
    this.comunicationService.myEmitter.subscribe(data => {
      this.data2Form = {option:'edit',title:'Editar curso',data:this.cursos[data],ind:data};
      //this.data2Form.data = this.cursos[data];
    });
    this.comunicationService.deleteEmitter.subscribe(data => {
      console.log("Eliminar = ",data);
      this.cursos.splice(data,1);
    });
  }
  setCurso(auxCurso:any){
    this.cursos.push({nombre:auxCurso.nombre,descripcion:auxCurso.descripcion,imagen:auxCurso.imagen});
  }
  editCurso(data){
    console.log(data);
    this.cursos[this.data2Form.ind] = data;
    this.onCancel();
  }
  onCancel(){
    this.data2Form = {option:'add',title:'Nuevo curso',data:null,ind:0};
  }

}

//ITEM
@Component({
  selector:'item-curso',
  template:`
    <mat-card class="example-card">
      <mat-card-header>
        <div mat-card-avatar class="example-header-image"></div>
        <mat-card-title>{{curso.nombre}}</mat-card-title>
        <mat-card-subtitle></mat-card-subtitle>
      </mat-card-header>
      <img mat-card-image [src]="curso.imagen" alt="Photo of a Shiba Inu">
      <mat-card-content>
        <p>{{curso.descripcion}}</p>
      </mat-card-content>
      <mat-card-actions>
        <button mat-button (click)="sendData()">Editar</button>
        <button mat-button (click)="onDelete()">Eliminar</button>
      </mat-card-actions>
    </mat-card>
  `
})
export class ItemCurso implements OnInit{
  @Input()
  curso:Curso;
  @Input()
  index:number;
  constructor(private comunicationService:ComunicationService){

  }
  ngOnInit(){

  }
  sendData(){
    console.log(this.index);
    
    this.comunicationService.pasoValor(this.index);
  }
  onDelete(){
    this.comunicationService.onDeleteEmitter(this.index);
  }
}

//LISTA
@Component({
  selector:'lista-curso',
  template:`
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h2>Listado de cursos</h2>
        </div>
      </div>  
      <div class="row">
        <div class="col-12 col-sm-3" *ngFor="let curso of cursos;index as i">
          <item-curso [curso]="curso" [index]="i"></item-curso>
        </div>
      </div>
    </div>
  `,  
  styles:[`
    .example-card {
      max-width: 400px;
    }
    
    .example-header-image {
      background-image: url('https://josefacchin.com/wp-content/uploads/2016/09/como-crear-curso-online-2.png');
      background-size: cover;
    }
  `]
})
export class ListaCurso implements OnInit{
  @Input()
  cursos:Curso;  
  constructor(){}
  ngOnInit(){    
  }
}

//FORM CREAR
@Component({
  selector:'crear-curso',
  template:`
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h2>{{data.title}}</h2>
        </div>
      </div>      
      <div class="row">
        <div class="col-12 col-sm-12">
          <form [formGroup]='myFormGroup'>
            <mat-form-field class="example-full-width">
              <input matInput placeholder="Nombre" formControlName="nombre">
            </mat-form-field>
            <br>
            <mat-form-field class="example-full-width">
              <input matInput placeholder="Descripcion" formControlName="descripcion">
            </mat-form-field>
            <br>
            <mat-form-field class="example-full-width">
              <input matInput placeholder="url imagen" formControlName="imagen">
            </mat-form-field>
            <br>
            <button *ngIf="data.option=='add'" mat-raised-button color="primary" [disabled]="!myFormGroup.valid" (click)="InsertData()">Guardar</button>
            <button *ngIf="data.option=='edit'" mat-raised-button color="primary" [disabled]="!myFormGroup.valid" (click)="EditData()">Actualizar</button>
            <button *ngIf="data.option=='edit'" mat-raised-button color="warn" (click)="onCancel()">Cancelar</button>
          </form>
        </div>
      </div>
    </div>
  `
})
export class CrearCurso implements OnInit{
  data:any;
  @Output() myEmitter = new EventEmitter<Curso>();
  @Output() editEmitter = new EventEmitter<any>();
  @Output() cancelEmitter = new EventEmitter<any>();
  @Input()
  set currentData(data:any){
    this.data = data;
    if(data.option=="edit" && data.data){
      this.myFormGroup.setValue(data.data);
    }
     
  }
  myFormGroup:FormGroup;
  
  constructor(private formBuilder:FormBuilder){
    this.myFormGroup = this.formBuilder.group({
      nombre: ['',Validators.required],
      descripcion: ['',Validators.required],
      imagen: ['',Validators.required]
    });
  }
  ngOnInit(){    
    //this.myFormGroup.get('descripcion').setValue('Framework PHP');
  }
  InsertData(){    
    this.myEmitter.emit(this.myFormGroup.value);
    this.myFormGroup.reset();
  }
  EditData(){    
    this.editEmitter.emit(this.myFormGroup.value);
    this.myFormGroup.reset();
  }
  onCancel(){
    this.myFormGroup.reset();
    this.cancelEmitter.emit();
  }
}
