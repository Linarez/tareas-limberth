import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { Tarea1Component } from './tarea1/tarea1.component';
import { Tarea2Component } from './tarea2/tarea2.component';

const routes: Routes = [  
  {
    path:'',
    component:InicioComponent,children:[
      {
        path:'tarea1',
        component:Tarea1Component,    
      },
      {
        path:'tarea2',
        component:Tarea2Component
      },
      {
        path:'',
        redirectTo:'tarea1',
        pathMatch:'full'
      }
    ]
  }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TareasRoutingModule { }
