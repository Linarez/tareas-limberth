import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ComunicationService {
  //data:any=null;
  @Output() myEmitter: EventEmitter<any> = new EventEmitter();
  @Output() deleteEmitter: EventEmitter<any> = new EventEmitter();

  constructor() {}
  
  pasoValor(param:any){
    //this.data = param;
    //this.myEmitter.emit(this.data);
    this.myEmitter.emit(param);
  }
  onDeleteEmitter(param:any){
    this.deleteEmitter.emit(param);
  }
}
